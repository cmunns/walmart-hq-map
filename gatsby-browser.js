/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

// You can delete this file if you're not using it
import './src/components/layout.scss'

export const onRouteUpdate = ({ location, prevLocation }) => {
  const focusWrapper = document.querySelector("#gatsby-focus-wrapper")
  if (focusWrapper) {
    focusWrapper.tabIndex = 1;
  }
}