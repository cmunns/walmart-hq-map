FROM --platform=linux/amd64 node:20-alpine AS build

WORKDIR /app
COPY . .

# Install python/pip
ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache python3
RUN apk add gcc musl-dev

# Install make etc.
RUN apk add build-base
RUN apk add vips-dev

RUN yarn global add node-gyp
RUN yarn
# RUN yarn config set nodeLinker node-modules
RUN yarn add sharp --cpu=amd64 --os=linux --libc=musl
RUN yarn build

RUN adduser appuser --uid 10000 && chown -R appuser:appuser /app
USER 10000

FROM nginx:alpine AS deploy

WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=build /app/public .
ENTRYPOINT ["nginx", "-g", "daemon off;"]