module.exports = {
  siteMetadata: {
    title: `Walmart Home Office Map`,
    description: `Map interaction for the new Bentonville Walmart HQ`,
    siteUrl: `https://corporate.walmart.com`,
    author: `Adam Munns`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-plugin-image`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: /\.inline\.svg$/
        }
      }
    },
    {
      resolve: 'gatsby-plugin-svgr',
      options: {
        prettier: true,         // use prettier to format JS code output (default)
        svgo: true,             // use svgo to optimize SVGs (default)
        svgoConfig: {
          removeViewBox: true, // remove viewBox when possible (default)
          cleanupIDs: false,    // remove unused IDs and minify remaining IDs (default)
        },
      },
    },
    {
      resolve: `gatsby-plugin-sass`,
    },
    {
      resolve: `gatsby-plugin-lodash`,
    },
    {
      resolve: `gatsby-plugin-google-gtag`,
      options: {
        // You can add multiple tracking ids and a pageview event will be fired for all of them.
        trackingIds: [
          "GA-281628584", // Google Analytics / GA
          "G-HMJ3ZBRYHV"
        ],
        // This object gets passed directly to the gtag config command
        // This config will be shared across all trackingIds
        gtagConfig: {
          anonymize_ip: true,
          cookie_expires: 0,
        },
        // This object is used for configuration specific to this plugin
        pluginConfig: {
          // Puts tracking script in the head instead of the body
          head: true
        },
      }
    }
    // {
    //   resolve: `gatsby-plugin-gatsby-cloud`,
    //   options: {
    //     headers: {
    //       allPageHeaders: [
    //         "X-Frame-Options: ALLOWALL",
    //       ],
    //     },
    //     allPageHeaders: [],
    //     mergeSecurityHeaders: false,
    //     mergeLinkHeaders: true,
    //     mergeCachingHeaders: true,
    //     transformHeaders: (headers, path) => headers
    //   },
    // },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
