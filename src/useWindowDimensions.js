import { useState, useEffect } from 'react';

function getWindowDimensions() {
  if( typeof window === `undefined` ) return;
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height
  };
}

export default function useWindowDimensions() {
  const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

  useEffect(() => {
        // timeoutId for debounce mechanism
    let timeoutId = null;
    const handleResize = () => {
      // prevent execution of previous setTimeout
      clearTimeout(timeoutId);
      // change width from the state object after 150 milliseconds
      timeoutId = setTimeout(() => setWindowDimensions(getWindowDimensions()), 150);
    };
    // set resize listener
    window.addEventListener('resize', handleResize);
    // window.addEventListener('orientationchange', handleResize);
    // clean up function
    return () => {
      // remove resize listener
      window.removeEventListener('resize', handleResize);
      // window.removeEventListener('orientationchange', handleResize);
    }
  }, []);

  return windowDimensions;
}