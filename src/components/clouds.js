import React from "react"
import { motion } from "framer-motion"

const Clouds = () => (
  <>
    {/* <motion.img
      tabIndex="-1"
      aria-hidden="true"
      animate={{ x: ["-20vw", "60vw"], opacity: [0, 0.8, 0.8, 0.8, 0.8, 0] }}
      transition={{ ease: "linear", duration: 30 }}
      initial={{ opacity: 0 }}
      exit={{ opacity: 0 }}
      src={`images/cloud-top-right.png`}
      className="cloud cloud1"
      style={{ position: "absolute", top: "0vh", maxHeight: `30vh` }}
    /> */}
    <motion.img
      tabIndex="-1"
      aria-hidden="true"
      animate={{ x: ["0vw", "100vw"], opacity: [0, 0.8, 0.8, 0.8, 0.8, 0] }}
      transition={{ ease: "linear", duration: 30 }}
      initial={{ opacity: 0 }}
      exit={{ opacity: 0 }}
      src={`images/cloud-top-left.png`}
      className="cloud cloud1"
      style={{ position: "absolute", top: "0vh", maxHeight: `30vh` }}
    />
    <motion.img
      tabIndex="-1"
      aria-hidden="true"
      animate={{ x: ["60%", "-100%"], opacity: [0, 0.8, 0.8, 0.8, 0.8, 0] }}
      transition={{ ease: "linear", duration: 40 }}
      initial={{ opacity: 0 }}
      exit={{ opacity: 0 }}
      src={`images/cloud-bottom.png`}
      className="cloud cloud1"
      style={{ position: "absolute", bottom: "0vh", maxHeight: `40vh` }}
    />
  </>
)

export default Clouds
