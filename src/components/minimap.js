import React, { useState } from "react"
import { motion } from "framer-motion"

const Minimap = ({
  setTransform,
  setMiniMapHover,
  miniMapHover,
  miniMapTransform,
  data,
}) => {
  const [miniMapOpen, setMiniMapOpen] = useState(false)
  const variants = {
    miniVisible: { height: `auto`, opacity: 1 },
    miniHidden: { height: `0px`, opacity: 0 },
  }

  return (
    <div className="minimap" tabIndex="-1" aria-hidden="true">
      <div className="minimap-header">
        <button
          className="minimap-toggle"
          tabIndex="-1"
          onClick={() => setMiniMapOpen(!miniMapOpen)}
        >
          <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17"><title>menu</title><g fill="#2E4055"><path d="M16 3v2H1V3h15zM1 10h15V8H1v2zm0 5h15v-2H1v2z" fill="#2E4055"></path></g></svg>
          <span className="minimap-toggle-active">Campus View By Quadrant</span>
        </button>
      </div>
      {miniMapOpen}
      <motion.div
        className="minimap-content"
        animate={miniMapOpen ? "miniVisible" : "miniHidden"}
        variants={variants}
        initial={{ height: `0px` }}
        transition={{
          type: "spring",
          stiffness: 20,
          damping: 10,
          mass: 0.2,
        }}
      >
        <div // eslint-disable-line jsx-a11y/no-static-element-interactions
          className="minimap-squares"
          onClick={e => {
            miniMapTransform(e, setTransform)
            setMiniMapOpen(false)
          }}
          onKeyPress={e => {
            miniMapTransform(e, setTransform)
            setMiniMapOpen(false)
          }}
          style={{ backgroundImage: `url('images/hq-map-mini.jpg')` }}
        >
          <button
            aria-label="North West Quad"
            tabIndex="-1"
            className={miniMapHover && miniMapHover.id === 1 ? "active" : ""}
            onMouseEnter={() => setMiniMapHover(data.quadrants[0])}
            onClick={() => {
              setMiniMapHover(data.quadrants[0])
            }}
          ></button>
          <button
            aria-label="North East Quad"
            tabIndex="-1"
            className={miniMapHover && miniMapHover.id === 2 ? "active" : ""}
            onMouseEnter={() => setMiniMapHover(data.quadrants[1])}
            onClick={() => setMiniMapHover(data.quadrants[1])}
          ></button>
          <button
            aria-label="South West Quad"
            tabIndex="-1"
            className={miniMapHover && miniMapHover.id === 3 ? "active" : ""}
            onMouseEnter={() => setMiniMapHover(data.quadrants[2])}
            onClick={() => setMiniMapHover(data.quadrants[2])}
          ></button>
          <button
            aria-label="South East Quad"
            tabIndex="-1"
            className={miniMapHover && miniMapHover.id === 4 ? "active" : ""}
            onMouseEnter={() => setMiniMapHover(data.quadrants[3])}
            onClick={() => setMiniMapHover(data.quadrants[3])}
          ></button>
        </div>
        <div className="minimap-info">
          {/* <h5>{miniMapHover && miniMapHover.name}</h5> */}
          <div>
            <span>{miniMapHover && miniMapHover.name}</span>
            <span>{miniMapHover && miniMapHover.link}</span>
          </div>
        </div>
      </motion.div>
    </div>
  )
}

export default Minimap
