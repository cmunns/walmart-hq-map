import React, { useState, useEffect } from "react"
import JSONData from "../data/pois.json"
import { motion } from "framer-motion"
import Image from "../components/image"
import getWindowDimensions from "../useWindowDimensions"

const MarkersExternal = ({
  activeItem,
  updateActiveItem,
  poi
}) => {
  const windowSize = getWindowDimensions()
  const [markersExternal, setMarkersExternal] = useState([])
  const poiName = poi && poi.name
  const variants = {
    visible: { opacity: 1, y: `-6px` },
    hidden: { opacity: 0, y: `0px` },
    pinVisible: { y: `-6px`, zIndex: 1000 },
    pinHidden: {
      y: `0px`,
      zIndex: 0,
      transition: {
        delay: 0.2,
      },
    },
    pinLoaded: i => ({
      y: `0px`,
      opacity: 1,
      transition: {
        delay: i * 0.3 + 1,
      },
    }),
  }

  function setMarkerPositions() {
    const markerList = JSONData.external.map((marker, index) => {
      const markerEl = document.getElementById(`prefix__${marker.id}`)
      const markerBB = markerEl
        ? markerEl.getBoundingClientRect()
        : { x: 0, y: 0 }
      // console.log(markerBB)
      return {
        id: marker.id,
        name: `${marker.name}`,
        image: `${marker.image}`,
        x: markerBB.x + markerBB.width / 2,
        y: markerBB.y,
        desc: marker.description
      }
    })
    setMarkersExternal(markerList)
  }

  useEffect(() => {
    setMarkerPositions()
    // set resize listener
    window.addEventListener("resize", setMarkerPositions)
  }, [])

  return (
    <div className="markers-external" data-active={activeItem} style={{
      left: (windowSize.width < 420) ? (8000 / 4504) * windowSize.height / 3 : 0
    }}>
      {markersExternal.map((marker, index) => (
        <button
          key={index}
          className={
            activeItem === marker.id
              ? "marker-external active"
              : "marker-external"
          }
          id={marker.id}
          onMouseEnter={e => updateActiveItem(e, marker.id)}
          onMouseLeave={e => updateActiveItem(null)}
          style={{ position: `absolute`, left: marker.x, top: marker.y }}
        >
          <motion.div
            initial={{ y: `-20px`, opacity: 0 }}
            custom={index}
            variants={variants}
            animate={
              activeItem === marker.id || poiName === marker.name
                ? "pinVisible"
                : !loaded
                  ? "pinLoaded"
                  : "pinHidden"
            }
          >
            {marker.image ? <Image src={marker.image} alt={marker.name} /> : ``}

            <motion.div
              className="tooltip"
              data-position={marker.y < 400 ? `bottom` : `top`}
              variants={variants}
              animate={activeItem === marker.id ? "visible" : "hidden"}
              initial={"hidden"}
              transition={{
                type: "spring",
                stiffness: 20,
                damping: 10,
                mass: 0.2,
              }}
            >
              <div key="Tooltip" className="tooltip-inner">
                <p><strong>{marker.name}</strong><br />{marker.desc}</p>
              </div>
            </motion.div>
          </motion.div>
        </button>
      ))}
    </div>
  )
}

export default MarkersExternal
