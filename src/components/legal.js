import React, {useState} from "react"
import { motion } from "framer-motion"

const Legal = () => {

    const [clicked, setClicked] = useState(false)
    const variants = {
        popen: { opacity: 1, display: "block" },
        pclosed: { opacity: 0, transitionEnd: { display: "none" } },
        fadeIn: { y: "0%" },
        fadeOut: { y: "100%" }
    }

    function clickToggle() {
        setClicked(!clicked)
    }

    return(
        <>
            <motion.img 
                className="legal-toggle"
                src={`images/pin-blue.svg`} width={`30px`} height={`30px`} 
                onClick={clickToggle}/>
            <motion.div 
                className="legal"
                variants={variants}
                animate={clicked ? "fadeIn" : "fadeOut"}
                transition={{ 
                    type: "spring", 
                    stiffness: 20,
                    damping: 10,
                    mass: 0.2
                }}
                >
                <motion.p
                animate={clicked ? "popen" : "pclosed"}
                variants={variants}
                transition={{ 
                    type: "spring", 
                    stiffness: 20,
                    damping: 10,
                    mass: 0.2
                }}
                >We're always evolving – listening to feedback and reevaluating our plans. Everything on this site is a work in progress and is subject to change.</motion.p>
            </motion.div>
        </>
    )
}

export default Legal