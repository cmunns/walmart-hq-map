import React, { useState, useEffect } from "react"
import JSONData from "../data/pois.json"
import getWindowDimensions from "../useWindowDimensions"


const Markers = ({ buildings, activeItem, poi, scale }) => {
    const windowSize = getWindowDimensions()
    const [markers, setMarkers] = useState([])
    const variants = {
        flagVisible: {
            opacity: 1,
            transition: {
                delay: .1,
            }
        },
        flagHidden: { opacity: 0 },
        pinVisible: { y: `-6px`, zIndex: 100000 },
        pinHidden: {
            y: `0px`,
            zIndex: 0,
            transition: {
                delay: 0
            }
        },
        pinLoaded: i => ({
            y: `0px`,
            opacity: 1,
            transition: {
                delay: (i * 0.15) + 1,
            }
        })
    }

    function setMarkerPositions() {
        const markerList = JSONData.pois.map((building, index) => {
            const buildingEl = document.getElementById(`prefix__${building.marker}`)
            const buildingBB = (buildingEl) ? buildingEl.getBoundingClientRect() : { x: 0, y: 0 }
            return {
                id: building.id,
                name: `${building.name}`,
                x: buildingBB.x + (buildingBB.width / 2),
                y: buildingBB.y
            }
        })
        setMarkers(markerList)
    }

    useEffect(() => {
        setMarkerPositions()
        // set resize listener
        window.addEventListener('resize', function () {
            // console.log( scale );
            // if( scale <= 1 ) {
            setMarkerPositions()
            // }
        });
        window.addEventListener('orientationchange', function () {
            window.location.reload();
        })
    }, [])


    return (
        <div className="markers" data-active={activeItem} style={{
            left: (windowSize.width < 420) ? (8000 / 4504) * windowSize.height / 3 : 0
        }}>
            {/* 
            {markers.map((marker, index) => (
                <div className={(activeItem === marker.id) ? 'marker active' : 'marker'} key={index} id={marker.id} style={{position: `absolute`, left: marker.x, top: marker.y}}
                    >
                    <div style={{
                            position: `absolute`,
                            left: `-8px`,
                            top: `-22px`
                        }} 
                        className={(activeItem === marker.id) ? "pinVisible" : (!loaded) ? "pinLoaded" : "pinHidden"}
                    >
                        <img width="15px" src={`images/marker.png`} alt="pin"/>
                        <span className={(activeItem === marker.id) ? "flag flagVisible" : "flag flagHidden"}>
                            {marker.name}
                        </span>
                    </div>
                </div>
            ))}
             */}
        </div>
    )
}

export default Markers