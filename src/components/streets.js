import React, { useState, useEffect } from "react"
import JSONData from "../data/pois.json"
import getWindowDimensions from "../useWindowDimensions"

const Streets = ({scale}) => {
    const windowSize = getWindowDimensions()
    const [streets, setStreets] = useState([])
    
    function setStreetPositions() {
        const markerKeys = JSONData.streets
        const markerList = markerKeys.map((street, index) => {
            const markerEl = document.querySelector(`#prefix__street #prefix__${street.id}`)
            const markerBB = (markerEl) ? markerEl.getBoundingClientRect() : {x:0, y:0}
            return {
                id: street.id,
                name: `${street.name}`,
                x: markerBB.x - (markerBB.width/2),
                y: markerBB.y - (markerBB.height/2),
                axis: street.axis
            }
        })
        setStreets(markerList)
    }

    useEffect(() => {
        setStreetPositions()
        // set resize listener
        window.addEventListener('resize', setStreetPositions);
        // window.addEventListener('orientationchange', setStreetPositions);
    }, [])

  return (
    <div className="streets" 
        tabIndex="-1"
        style={{
            left: (windowSize.width < 420 ) ? (8000/4504) * windowSize.height / 3 : 0
        }}>
     {streets.map((street, index) => (
        <div 
        className={(scale >= 1.5) ? "street visible" : "street hidden"}
        key={index} 
        style={{
            position: `absolute`, zIndex: `1000`, left: street.x, top: street.y, transform: `rotate(${street.axis}deg)`
        }}
        >
            <span className="street-sign">
                {street.name}
            </span>
        </div>
     ))}
     </div>
  )
}

export default Streets
