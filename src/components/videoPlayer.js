import React, { useEffect, useRef } from 'react'
import videojs from 'video.js'
import 'video.js/dist/video-js.css'

const VideoPlayer = ({ key, options, placeholder, onPlay, onEnded, isMuted }) => {
    const container = useRef()
    const player = useRef()


    useEffect(() => {
        player.current = videojs(container.current, options)
        return () => {
            player.current = null
        }
    }, [key])

    return (
        <>
            <div data-vjs-player key={key}>
                <video
                    poster={placeholder}
                    ref={container}
                    muted={isMuted}
                    controls={false}
                    autoPlay={true}
                    onPlay={onPlay}
                    onEnded={onEnded}
                    playsInline={true}
                    aria-label='Fitness Center Video Tour'
                />
            </div>
        </>
    )
}

export default VideoPlayer