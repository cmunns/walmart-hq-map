import React, { useMemo } from "react"
import { useStaticQuery, graphql } from "gatsby"
import { GatsbyImage } from "gatsby-plugin-image";

/*
 * This component is built using `gatsby-image` to automatically serve optimized
 * images with lazy loading and reduced file sizes. The image is loaded using a
 * `useStaticQuery`, which allows us to load the image from directly within this
 * component, rather than having to pass the image data down from pages.
 *
 * For more information, see the docs:
 * - `gatsby-image`: https://gatsby.dev/gatsby-image
 * - `useStaticQuery`: https://www.gatsbyjs.org/docs/use-static-query/
 */

const Image = ({ src, ...props }) => {
  const data = useStaticQuery(graphql`{
  allFile(filter: {internal: {mediaType: {regex: "images/"}}}) {
    edges {
      node {
        relativePath
        childImageSharp {
          gatsbyImageData(
            width: 800, 
            quality: 100, 
            formats: [AUTO, WEBP],
            layout: CONSTRAINED,
            placeholder: BLURRED,
          )
        }
      }
    }
  }
}`)

  const match = useMemo(() => (
    data.allFile.edges.find(({ node }) => src === node.relativePath)
  ), [data, src])

  const ImgOutput = () => {
    if (match) {
      return (
        <GatsbyImage
          image={match.node.childImageSharp.gatsbyImageData}
          {...props} />
      );
    } else {
      return `Image Not Found`
    }
  }

  return (
    ImgOutput()
  )
}

export default Image
