import React from "react"
import getWindowDimensions from "../useWindowDimensions"

const ZoomControls = ({
    setTransform, 
    setPositionX, 
    setPositionY, 
    setScale, 
    setIsAnimating, 
    scale
    }) => {
    
    const windowSize = getWindowDimensions()
    const width = windowSize.width
    const height = windowSize.height
    const positionRatio = (windowSize) ? (8000/4504 * height) : 1
    // let defaultPosX = (windowSize && width < positionRatio) ? -((positionRatio - width)/2) : 0

    const quads = document.getElementById(`quadrants`)
    const q = quads.getBoundingClientRect()
    let x, y
    const offsetRatioX = q.x / width 
    const offsetRatioY = q.y / height 
    const updatedOffsetX = ((width) - (q.width * 2.5)) / 2
    const updatedOffsetY = ((height) - (q.height * 2.5)) / 2
    const newEdgeX = (width*2.5) * offsetRatioX
    const newEdgeY = (height*2.5) * offsetRatioY
    x = -newEdgeX + updatedOffsetX
    y = -newEdgeY + updatedOffsetY
    
    const centerXOut = (windowSize && width < positionRatio) ? -((positionRatio - width)/2) : 0
    const centerXIn = (windowSize && width < positionRatio) ? -(positionRatio) : x

    // console.log(`x: ${x}, y: ${y}, scale: ${scale}, centerXOut: ${centerXOut}, centerXIn: ${centerXIn}`)

    return (
        <div className="zoom-controls" aria-hidden="true">
            <button 
            disabled={scale > 1.5}
            aria-label="Zoom In"
            onClick={() => {
                setTimeout(() => {
                    setIsAnimating(true)
                    setTimeout(() => {
                        setIsAnimating(false)
                    }, 800)
                    setPositionX(x)
                    setPositionY(x)
                    setScale(2.5)
                    setTimeout(() => {
                    setTransform(centerXIn, y, 2.5, 800, "easeInOutQuad")
                    }, 50)
                }, 10)
            }}>&#43;</button>
            <button 
            disabled={scale < 1.5}
            aria-label="Zoom Out"
            onClick={() => {
                setTimeout(() => {
                    setIsAnimating(true)
                    setTimeout(() => {
                        setIsAnimating(false)
                    }, 800)
                    setScale(1)
                    setPositionX(x)
                    setPositionY(x)
                    setTimeout(() => {
                        setTransform(centerXOut, 0, 1, 800, "easeInOutQuad")
                    }, 50)
                }, 10)
            }}>&#8722;</button>
        </div>
    )
}

export default ZoomControls