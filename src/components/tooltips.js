import React, { useState, useEffect } from "react"
import JSONData from "../data/pois.json"
import { motion } from "framer-motion"
import getWindowDimensions from "../useWindowDimensions"


const Tooltips = ({ tooltip, showTooltip, scale, width }) => {
  const windowSize = getWindowDimensions()
  const [markers, setMarkers] = useState([])
  const variants = {
    visible: { opacity: 1, y: (tooltip && tooltip.y < 300) ? `6px` : `-6px` },
    hidden: { opacity: 0, y: `0px`, transition: { duration: 0 } },
    loaded: i => ({
      y: `0px`,
      opacity: 1,
      transition: {
        delay: (i * 0.1) + 1,
      }
    }),
    grow: { scale: 1.2 },
    shrink: { scale: 1 }
  }

  function setTooltipPositions() {
    const markerKeys = JSONData.infoMarkers
    const markerList = markerKeys.map((infoMarker, index) => {
      const markerEl = document.getElementById(`prefix__${infoMarker.id}`)
      const markerBB = (markerEl) ? markerEl.getBoundingClientRect() : { x: 0, y: 0 }
      return {
        id: infoMarker.id,
        name: `${infoMarker.name}`,
        desc: `${infoMarker.description}`,
        x: markerBB.x + (markerBB.width / 2),
        y: markerBB.y
      }
    })
    setMarkers(markerList)
  }

  useEffect(() => {
    setTooltipPositions()
    // set resize listener
    window.addEventListener('resize', setTooltipPositions)
    window.addEventListener('orientationchange', function () {
      // window.dispatchEvent(new Event('resize'));
      // setTooltipPositions()
    })
  }, [windowSize.width])


  return (
    <div className={(scale >= 1.5) ? "tooltips visible" : "tooltips hidden"} style={{
      left: (windowSize.width < 420) ? (8000 / 4504) * windowSize.height / 3 : 0
    }}>
      {markers.map((marker, index) => (
        <div
          className={(tooltip && tooltip.id === marker.id) ? 'tooltip active' : 'tooltip'}
          id={marker.id} key={index}
          style={{ position: `absolute`, left: marker.x, top: marker.y }}
        >
          <div>
            <div key="Tooltip"
              className="tooltip-inner"
              data-position-x={tooltip && width - tooltip.x < 300 ? `left` : `right`}
              data-position-y={tooltip && tooltip.y < 300 ? `bottom` : (tooltip) ? `top` : ``}>
              <p>{marker.desc}</p>
            </div>
          </div>
          <img
            onMouseEnter={e => showTooltip(e, marker.id)}
            // onTap={e => showTooltip(e, marker.id)}
            onMouseLeave={e => showTooltip(null)}
            width={10}
            src={`images/pin.png`} />
        </div>

      ))}
    </div>
  )
}

export default Tooltips
