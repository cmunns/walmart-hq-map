import React, {useState, useEffect} from "react"
import { motion, AnimatePresence } from "framer-motion"
import Image from "../components/image"
import getWindowDimensions from "../useWindowDimensions"


function Tooltip({ tooltip, positionX, positionY }) {
  const [index, setIndex] = useState(0)
  const [description, setDescription] = useState("")
  const windowSize = getWindowDimensions()

  useEffect(() => {
    setIndex(0)
    // setDescription('Test')
  },[])

  return (
    <AnimatePresence>
      {tooltip.id != null && (
        <div 
          className="tooltip"  
          style={{position: `absolute`, left: Math.abs(tooltip.x + positionX), top: Math.abs(tooltip.y + positionY)}}
          >
            <div>
              <div key="Tooltip" 
                className="tooltip-inner" 
                data-position-x={tooltip && windowSize.width - tooltip.x < 300 ? `left` : `right`}
                data-position-y={tooltip && tooltip.y < 300 ? `bottom` : (tooltip) ? `top` : ``}>
                <p>{description}</p>
              </div>
            </div>
            {/* <img 
              width={10}
              src={`images/pin.png`} /> */}
        </div>
      )}
    </AnimatePresence>
  )
}

export default Tooltip
