/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import ScrollContainer from 'react-indiana-drag-scroll'
// import './layout.scss'

const Layout = ({ children }) => {
  return (
    <ScrollContainer vertical={true} horizontal={true} className="scroll-container">
      <main
        className="map"
        style={{
          height: this.props.zoom ? `calc( 100vh + (100vh * 0.6))` : `100vh`,
        }}
      >
        {children}
      </main>
    </ScrollContainer>
  )
}

export default Layout
