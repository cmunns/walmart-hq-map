import React from "react"

const FitnessVideoMap = ({ currentVideo, updateVideo }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      viewBox="0 0 1200 640.32"
      xmlSpace="preserve"
      aria-hidden="true"
      style={{ backgroundImage: `url('images/fitness-map-legend.jpg')` }}
    >
      <g id="commons" onClick={() => updateVideo('commons')} className={`${currentVideo.video.id == 'commons' ? 'active' : ''}`}>
        <ellipse className={`${currentVideo.video.id == 'commons' ? 'active' : ''}`} cx="649.3" cy="179" rx="59.5" ry="24.5" />
      </g>
      <g id="promenade" onClick={() => updateVideo('promenade')} className={`${currentVideo.video.id == 'promenade' ? 'active' : ''}`}>
        <path
          id="promenade"
          fill="white"
          d="M1071.6,320.9l-161.3-34.6L692.2,140.9l-151,21.4L624,247l23.3-8.2l13,8.2l21.6-8.9L791,286.3
		l44.4,13.7l50.6,16.6l197,41.5L1071.6,320.9z M649.3,203.5c-32.9,0-59.5-11-59.5-24.5s26.6-24.5,59.5-24.5s59.5,11,59.5,24.5
		S682.2,203.5,649.3,203.5z"
        />
      </g>
      <g id="fitness" onClick={() => updateVideo('fitness')} className={`${currentVideo.video.id == 'fitness' ? 'active' : ''}`}>
        <path
          id="fitness"
          fill="white"
          d="M214.8,180.24l324.48-52.56l15.12,6.24l46.08-13.44 l7.2,6.96l23.28-3.84l-1.68-5.28l35.76-5.52l38.4,25.44L540,162l-320.64,53.52L214.8,180.24z"
        />
        <path
          fill="white"
          d="M100.08,312.96L88.8,388.08l253.68-58.56l2.88,8.4l84.48-20.64 l198.24-54.24l-3.84-15.12l-33.36-35.76L100.08,312.96z"
        />
        <path
          fill="white"
          d="M749.28,268.08l-13.44,12.24l-30-0.96l-26.4,18.48l-16.8,25.68 l70.08,29.28l49.92-40.32l2.64-13.68l-7.44-2.4l13.2-9.36L749.28,268.08z"
        />
        <path
          fill="white"
          d="M884.88,316.8l-5.52,12.96l-30.96,2.88l-10.8,25.44l2.4,35.52 l93.36,24.72l4.8-14.88l12-49.6  8l-4.8-8.16l-11.04-2.4l4.32-15.12L884.88,316.8z"
        />
      </g>
      <g id="pool" onClick={() => updateVideo('pool')} className={`${currentVideo.video.id == 'pool' ? 'active' : ''}`}>
        <path
          onClick={() => updateVideo('pool')}
          fill="white"
          d="M31.2,248.88L539.04,162l53.28,50.16L4.56,332.4 L31.2,248.88z"
        />
      </g>
      <g id="tennis" onClick={() => updateVideo('tennis')} className={`${currentVideo.video.id == 'tennis' ? 'active' : ''}`}>
        <path
          id="indoor-courts"
          fill="white"
          d="M725.52,164.16l142.08-28.32l285.84,142.56 l-119.28,35.28l-121.68-26.64L725.52,164.16z"
        />
        <path
          id="racquet"
          fill="white"
          d="M690.72,48.48L783.84,36l417.36,191.52l-0.24,52.32 l-43.2,8.88l1.2-8.64L690.72,48.48z"
        />
      </g>
      <path
        fill="white"
        d="M692.16,141.6l139.92-24.96l36.48,18.48L723.6,163.44 L692.16,141.6z"
      />
      {/* <g onClick={() => updateVideo('fitness')}> */}
      <path
        id="gym"
        fill="white"
        d="M614.16,79.2l72.24-10.32l5.76,3.6l34.32-5.28l103.92,49.44 l-125.76,22.08L614.16,79.2z"
      />
      {/* </g> */}
    </svg>
  )
}

export default FitnessVideoMap
