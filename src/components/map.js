import React from "react"
import JSONData from "../data/pois.json"
import getWindowDimensions from "../useWindowDimensions"
import Streets from "./streets"

const SvgComponent = ({ updateActiveItem, scale, ...props }) => {
  const windowSize = getWindowDimensions()

  const showTooltip = (e, id) => {
    const svg = document.getElementById('map');
    const tooltip = e.target.closest('.tooltip')
    const classX = (windowSize.width - e.clientX < 300) ? 'left' : 'right'
    const classY = (e.clientY < 300) ? 'bottom' : 'top'
    document.querySelectorAll(`.tooltip`).forEach(function (element) {
      element.classList.remove(`active`);
    });
    setTimeout(() => {
      tooltip.classList.add(`active`, `${classX}`, `${classY}`)
    }, 0)
    svg.appendChild(tooltip);
  }

  const hideTooltip = (e, parent) => {
    const group = e.target.closest('g.tooltip')
    const parentGroup = document.getElementById(parent);
    parentGroup.appendChild(group);
    const tooltip = parentGroup.querySelector('.active')
    if (tooltip) tooltip.classList.remove(`active`, `top`, `bottom`, `left`, `right`)
  }

  return (
    <svg
      id="map"
      x={0}
      y={0}
      title="Walmart New Home Office Campus"
      viewBox="0 0 8000 4504"
      xmlSpace="preserve"
      {...props}
    // ref={ref}
    >
      <style>
        {
          ".prefix__st0{fill:none;stroke:#075b93;stroke-width:7;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10}.prefix__st1{fill:#f89a1c}.prefix__st2{fill:#e2e2e2}.prefix__st3{fill:#1e2ef7}.prefix__st4{fill:#cc4e27}.prefix__st7,.prefix__st9{fill:#075b93}.prefix__st9{stroke:#075b93;stroke-width:7;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10}"
        }
      </style>
      <g id="quadrants">
        <path
          id="quadrant4"
          className="prefix__st0"
          d="M3357.2 2134.4s144.2-113.4 165.2-135.2c21-21.8 110.4-79.2 337.8-113.7 80.4-12.2 156.3-145.7 70.5-234.3-48.3-49.8-13.1-188-13.1-188 2.6-14.3 10.4-20.9 24.1-13.4l1317.5 496c14.9 5 18.9 24.2 7.2 34.7l-664.6 802.7c-5.9 5.3-14.3 6.8-21.7 3.8l-1218.5-617c-14.8-6.1-17.2-26.1-4.4-35.6z"
        />
        <path
          id="quadrant3"
          className="prefix__st0"
          d="M2230.8 1614.1l741.4-560.9c5.4-4 12.5-5.1 18.9-3l873.7 364c22.9 10 11.1 29.5 8.5 49.1L3851 1595c-3 17.5 2.6 35.4 15.1 48 30.1 30.4 79.7 95.3 45 166.8-33.3 68.7-274.3 47.6-380.5 147.3l-212.9 176.1c-5.9 5.3-14.3 6.8-21.7 3.8l-1060.8-487.4c-14.8-6.1-17.3-26-4.4-35.5z"
        />
        <path
          id="quadrant2"
          className="prefix__st0"
          d="M4147.5 1435.1l643.9-569.6c5.4-4 12.5-5.1 18.9-3l1084.8 369.2c14.9 5 18.9 24.2 7.2 34.7L5318.5 1918c-5.9 5.3-14.3 6.8-21.7 3.8l-1144.9-451.2c-14.8-6.1-17.2-26-4.4-35.5z"
        />
        <path
          id="quadrant1"
          className="prefix__st0"
          d="M3078.9 1007.7l651.5-482.8c5.4-4 12.5-5.1 18.9-3L4714.4 833c14.9 5 18.9 24.2 7.2 34.7l-645.8 572.5c-5.9 5.3-14.3 6.8-21.7 3.8l-970.8-400.8c-14.8-6.1-17.2-26-4.4-35.5z"
        />
      </g>

      <g id="streetLabels" aria-hidden="true">
        <g className="street-sign-g" x={6826.7} y={2924} style={{ transform: "rotate(-65deg) skew(8deg, 0)" }}>
          <foreignObject x={6826.7} y={2924} width={200} height={30}>
            <span className="street-sign" xmlns="http://www.w3.org/1999/xhtml">
              Moberly Lane
            </span>
          </foreignObject>
        </g>
        <g className="street-sign-g" x={1628} y={3233} style={{transform: "rotate(33deg) skew(8deg, 0)"}}>
          <foreignObject x={1628} y={3233} width={200} height={30}>
            <span
              className="street-sign"
              xmlns="http://www.w3.org/1999/xhtml"
            >
              14th Street
            </span>
          </foreignObject>
        </g>
        <g className="street-sign-g" x={3380} y={761} style={{transform: "rotate(-37deg) skew(8deg, 0)"}}>
          <foreignObject x={3380} y={761} width={200} height={30}>
            <span
              className="street-sign"
              xmlns="http://www.w3.org/1999/xhtml"
            >
              J Street
            </span>
          </foreignObject>
        </g>
        <g className="street-sign-g" x={4427.4} y={1168} style={{transform: "rotate(-44deg) skew(8deg, 0)"}}>
          <foreignObject x={4427.4} y={1168} width={200} height={30}>
            <span
              className="street-sign"
              xmlns="http://www.w3.org/1999/xhtml"
            >
              S.E. P Street
            </span>
          </foreignObject>
        </g>
        <g className="street-sign-g" x={4940} y={2430} style={{transform: "rotate(-51deg) skew(8deg, 0)"}}>
          <foreignObject x={4940} y={2430} width={200} height={30}>
            <span
              className="street-sign"
              xmlns="http://www.w3.org/1999/xhtml"
            >
              Martin Luther King, Jr., Parkway
            </span>
          </foreignObject>
        </g>
        <g className="street-sign-g" x={2853.4} y={1948} style={{transform: "rotate(25deg) skew(8deg, 0)"}}>
          <foreignObject x={2853.4} y={1948} width={200} height={30}>
            <span
              className="street-sign"
              xmlns="http://www.w3.org/1999/xhtml"
            >
              10th Street
            </span>
          </foreignObject>
        </g>
        <g className="street-sign-g" x={4164.4} y={2589} style={{ transform: "rotate(26deg) skew(8deg, 0)" }}>
          <foreignObject x={4164.4} y={2589} width={200} height={30}>
            <span
              className="street-sign"
              xmlns="http://www.w3.org/1999/xhtml"
            >
              10th Street
            </span>
          </foreignObject>
        </g>
        <g className="street-sign-g" x={3980} y={1434} style={{ transform: "rotate(23deg) skew(8deg, 0)" }}>
          <foreignObject x={3980} y={1434} width={200} height={30}>
            <span
              className="street-sign"
              xmlns="http://www.w3.org/1999/xhtml"
            >
              8th Street
            </span>
          </foreignObject>
        </g>
        <g className="street-sign-g" x={4224.1} y={650} style={{transform: "rotate(18deg) skew(8deg, 0)"}}>
          <foreignObject x={4224.1} y={650} width={200} height={30}>
            <span
              className="street-sign"
              xmlns="http://www.w3.org/1999/xhtml"
            >
              5th Street
            </span>
          </foreignObject>
        </g>
        <g className="street-sign-g" x={5371.1} y={1020} style={{transform: "rotate(19deg) skew(8deg, 0)"}}>
          <foreignObject x={5371.1} y={1020} width={200} height={30}>
            <span
              className="street-sign"
              xmlns="http://www.w3.org/1999/xhtml"
            >
              5th Street
            </span>
          </foreignObject>
        </g>
        <g className="street-sign-g" x={5340.6} y={336} style={{transform: "rotate(15deg) skew(8deg, 0)"}}>
          <foreignObject x={5340.6} y={336} width={200} height={30}>
            <span
              className="street-sign"
              xmlns="http://www.w3.org/1999/xhtml"
            >
              Central Avenue
            </span>
          </foreignObject>
        </g>
        <g className="street-sign-g" x={5300.6} y={336} style={{transform: "rotate(-38deg) skew(8deg, 0)"}}>
          <foreignObject x={2774} y={1582} width={200} height={30}>
            <span
              className="street-sign"
              xmlns="http://www.w3.org/1999/xhtml"
            >
              S.E. Customer Blvd.
            </span>
          </foreignObject>
        </g>
        <g className="street-sign-g" x={5340.6} y={336} style={{transform: "rotate(-40deg) skew(8deg, 0)"}}>
          <foreignObject x={3094} y={1788} width={200} height={30}>
            <span
              className="street-sign"
              xmlns="http://www.w3.org/1999/xhtml"
            >
              S.E. Respect Ave.
            </span>
          </foreignObject>
        </g>
        <g className="street-sign-g" x={5340.6} y={336} style={{transform: "rotate(-46deg) skew(8deg, 0)"}}>
          <foreignObject x={4068} y={2050} width={200} height={30}>
            <span
              className="street-sign"
              xmlns="http://www.w3.org/1999/xhtml"
            >
              S.E. P Street
            </span>
          </foreignObject>
        </g>
        <g className="street-sign-g" x={5340.6} y={336} style={{transform: "rotate(-47deg) skew(8deg, 0)"}}>
          <foreignObject x={4780} y={1342} width={200} height={30}>
            <span
              className="street-sign"
              xmlns="http://www.w3.org/1999/xhtml"
            >
              S.E. Excellence Way
            </span>
          </foreignObject>
        </g>
        <g className="street-sign-g" x={5340.6} y={336} style={{transform: "rotate(-49deg) skew(8deg, 0)"}}>
          <foreignObject x={5112} y={1523} width={200} height={30}>
            <span
              className="street-sign"
              xmlns="http://www.w3.org/1999/xhtml"
            >
              S.E. Integrity Dr.
            </span>
          </foreignObject>
        </g>
      </g>

      <g
        id="structures"
        onMouseEnter={e => updateActiveItem(e)}
        onMouseLeave={e => updateActiveItem(e, "")}
        // onTouchStart={e => updateActiveItem(e)}
        onKeyPress={e => updateActiveItem(e)}
      >
        {JSONData.pois.map((building, index) => {
          let offsetX = (building.id === 'DGTB') ? (scale > 1.5 ? 6780 : 6440) : building.x;
          let bounce = (building.bounce) ? 'bouncing' : '';
          let featured = (building.featured) ? 'featured' : '';
          return (
            <g
              id={building.id}
              key={building.id}
              tabIndex={index + 1}
              className={`structure ${bounce} ${featured}`}
              role="button"
              aria-label={`${building.name}`}
            >
              <path className="prefix__st9" d={building.d} />
              <g className="pinDiv" x={building.x} y={building.y - 100}>
                <g className="pinDiv--content">
                  <foreignObject
                    x={offsetX}
                    y={building.y - 100}
                    width={2000}
                    height={2000}
                  >
                    <span className="pinDiv--flag">{building.name}</span>
                  </foreignObject>
                </g>
                <image href={"images/marker.svg"} width={70} height={110} className="pinDiv--img" x={building.x - 60} y={building.y - 124} />
              </g>
            </g>
          )
        })}
      </g>

      <g id="external">
        {JSONData.external.map((tooltip, index) => (
          <g
            id={tooltip.id}
            key={tooltip.id}
            tabIndex={
              JSONData.pois.length + JSONData.infoMarkers.length + index + 1
            }
            role="presentation"
            aria-label={tooltip.description}
            className="tooltip tooltip-external"
            onMouseOver={e => showTooltip(e)}
            onMouseOut={e => hideTooltip(e, "external")}
          // onTouchStart={e => showTooltip(e)}
          >
            <g className="tooltip-content">
              <foreignObject x={tooltip.x} y={tooltip.y} width={2000} height={2000}>
                <div className="tooltip-inner" xmlns="http://www.w3.org/1999/xhtml">
                  <p xmlns="http://www.w3.org/1999/xhtml">
                    <strong>{tooltip.name}</strong>
                  </p>
                  {tooltip.description}
                </div>
              </foreignObject>
            </g>
            <image href={`images/${tooltip.image}`} width={220} height={220} className="tooltip--img" x={tooltip.x - 110} y={tooltip.y - 110} />
          </g>
        ))}
      </g>

      <g id="infoMarkers">
        {JSONData.infoMarkers.map((tooltip, index) => (
          <g
            id={tooltip.id}
            key={tooltip.id}
            tabIndex={JSONData.pois.length + index + 1}
            aria-label={tooltip.description}
            role="presentation"
            className="tooltip"
            onMouseOver={e => showTooltip(e)}
            onMouseOut={e => hideTooltip(e, "infoMarkers")}
          // onTouchStart={e => showTooltip(e)}
          >
            <g className="tooltip-content" aria-hidden="true">
              <foreignObject x={tooltip.x} y={tooltip.y} width={2000} height={2000}>
                <div className="tooltip-inner" xmlns="http://www.w3.org/1999/xhtml">
                  <p xmlns="http://www.w3.org/1999/xhtml">{tooltip.description}</p>
                </div>
              </foreignObject>
            </g>
            <image href={"images/pin.svg"} width={50} height={50} className="tooltip--img" x={tooltip.x - 25} y={tooltip.y - 25} />
          </g>
        ))}
      </g>
    </svg>
  )
}

export default SvgComponent
