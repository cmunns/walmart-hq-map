/* eslint-disable */
import React, { useState, useEffect, useRef, useCallback, useLayoutEffect } from "react"
import { motion, AnimatePresence } from "framer-motion"
import Image from "../components/image"
import { ReactComponent as CloseBtn } from "../assets/close-outline.svg"
import { ReactComponent as PrevBtn } from "../assets/chevron-back-outline.svg"
import { ReactComponent as NextBtn } from "../assets/chevron-forward-outline.svg"
import VideoPlayer from "../components/videoPlayer";
import getWindowDimensions from "../useWindowDimensions"
import VideoData from "../data/videos.json"
import FitnessVideoMap from "../components/fitnessVideoMap"
import useSound from 'use-sound';
import useWindowFocus from 'use-window-focus';
import { isMobile } from 'react-device-detect';
import { getQueryString } from "../utils"

const MobileExplore = ({ updateVideo, exploreDropdownOpen, setExploreDropdownOpen }) => {
  const windowSize = getWindowDimensions()
  return (
    <div className="video-explore-container" aria-hidden={`${(windowSize && windowSize.width > 769) ? true : false}`}>
      <button id="mobileDropdown" className="video-explore-btn" aria-expanded={`${exploreDropdownOpen ? true : false}`} onClick={() => setExploreDropdownOpen(!exploreDropdownOpen)}>
        <span>Explore Spaces</span>
        <svg viewBox="0 0 12 12" xmlns="http://www.w3.org/2000/svg"><g transform="matrix(0.5,0,0,0.5,0,0)"><path d="M.541,5.627,11.666,18.2a.5.5,0,0,0,.749,0L23.541,5.627" fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round"></path></g></svg>
      </button>
      <ul className={`video-explore-opts ${exploreDropdownOpen ? 'fade-in' : ''}`} aria-labelledby="mobileDropdown" aria-hidden={`${exploreDropdownOpen ? false : true}`}>
        <li onClick={() => {
          updateVideo('commons')
        }}>Commons</li>
        <li onClick={() => {
          updateVideo('promenade')
        }}>Promenade</li>
        <li onClick={() => {
          updateVideo('pool')
        }}>Pool Area</li>
        <li onClick={() => {
          updateVideo('fitness')
        }}>Fitness</li>
        <li onClick={() => {
          updateVideo('tennis')
        }}>Indoor Courts</li>
        <li onClick={() => {
          updateVideo('exterior')
        }}>Exterior</li>
      </ul>
    </div >
  )
}

const VideoTourButtons = ({ updateVideo, currentVideo }) => {
  if (!currentVideo.path) return '';
  const vidObjPath = currentVideo.path
  const nextKeys = Object.keys(vidObjPath)
  if (!nextKeys) return '';
  return nextKeys.map((k, i) => {
    return <button key={i} className="video-action-btn" onClick={() => updateVideo(k)}>{VideoData['legend'][k]['button']}</button>
  })
}

const VideoOverlay = ({ classes, updateVideo, currentVideo, showVideoActions, exploreDropdownOpen, setExploreDropdownOpen }) => {
  return (
    <div className={`video-overlay ${currentVideo.video.id} ${classes}`}>
      <div className="video-header">

        <MobileExplore updateVideo={updateVideo} exploreDropdownOpen={exploreDropdownOpen} setExploreDropdownOpen={setExploreDropdownOpen} />

        <div className="video-header-inner" aria-labelledby={currentVideo.video.name} role="group" tabIndex={0}>
          <div>
            <h1 id={currentVideo.video.name} dangerouslySetInnerHTML={{ __html: currentVideo.video.name }}></h1>
            <div dangerouslySetInnerHTML={{ __html: currentVideo.video.desc }}></div>
          </div>
          <FitnessVideoMap currentVideo={currentVideo} updateVideo={updateVideo} />
        </div>
      </div>

      <div className={`video-actions ${showVideoActions ? 'fade-in' : ''}`}>

        {currentVideo.video.id != 'intro' && currentVideo.video.id != 'exterior' && (
          <h2>Where do you want to go next?</h2>
        )}

        <VideoTourButtons updateVideo={updateVideo} currentVideo={currentVideo} />

        {currentVideo.video.id == 'exterior' && (
          <button className="video-action-btn" tabIndex={0} onClick={() => updateVideo('intro')}>Start Over</button>
        )}

      </div>
    </div>
  )
}

function Modal({ data, show, closeModal }) {
  const [index, setIndex] = useState(0)
  const [currentVideo, setCurrentVideo] = useState({ video: null, path: null });
  const [videoHasFinished, setVideoHasFinished] = useState(false);
  const [videoFirstFive, setVideoFirstFive] = useState(false);
  const [isMuted, setIsMuted] = useState(false)
  const [exploreDropdownOpen, setExploreDropdownOpen] = useState(false);
  const [showVideoActions, setShowVideoActions] = useState(false);
  const modalRef = useRef(null)
  const [play, { pause, stop }] = useSound('/audio/ambient.mp3', { loop: true, volume: .18 });
  const windowSize = getWindowDimensions()
  const windowFocused = useWindowFocus();

  // const modalFull = (data && data.id == 'FITNESS') ? true : false;
  const modalFull = false;

  const handleNext = () => {
    index === data.images.length - 1 ? setIndex(0) : setIndex(index + 1)
  }
  const handlePrevious = () => {
    index === 0 ? setIndex(data.images.length - 1) : setIndex(index - 1)
  }

  function getAxisInitial() {
    return (windowSize && windowSize.width > 769) ? { x: `100%` } : { y: `100%` }
  }

  function getAxisAnimate() {
    return (windowSize && windowSize.width > 769) ? { x: `0%` } : { y: `0%` }
  }

  function getAxisExit() {
    return (windowSize && windowSize.width > 769) ? { x: `100%` } : { y: `100%` }
  }

  const modalKeyDown = (e, firstFocusableElement, lastFocusableElement) => {
    let isTabPressed = e.key === 'Tab' || e.keyCode === 9;
    if (!isTabPressed) {
      return;
    }

    if (e.shiftKey) { // if shift key pressed for shift + tab combination
      if (firstFocusableElement && document.activeElement === firstFocusableElement) {
        if (lastFocusableElement) lastFocusableElement.focus(); // add focus for the last focusable element
        e.preventDefault();
      }
    } else { // if tab key is pressed
      if (lastFocusableElement && document.activeElement === lastFocusableElement) { // if focused has reached to last focusable element then focus first focusable element after pressing tab
        if (firstFocusableElement) firstFocusableElement.focus(); // add focus for the first focusable element
        e.preventDefault();
      }
    }
  }

  const updateVideo = useCallback((id) => {
    const vidObj = VideoData['legend'][id]
    let vidPath = currentVideo.path ? currentVideo.path[id] : undefined

    // if (id === 'outro') vidPath = VideoData['paths']['intro']

    if (vidPath === undefined) {
      if (id === 'intro') vidPath = VideoData['paths']['intro']
      if (id === 'commons') vidPath = VideoData['paths']['intro']['commons']
      if (id === 'promenade') vidPath = VideoData['paths']['intro']['commons']['promenade']
      if (id === 'pool') vidPath = VideoData['paths']['intro']['commons']['pool']
      if (id === 'fitness') vidPath = VideoData['paths']['intro']['commons']['fitness']
      if (id === 'tennis') vidPath = VideoData['paths']['intro']['commons']['pool']['fitness']['tennis']
      if (id === 'exterior') vidPath = VideoData['paths']['intro']['commons']['pool']['fitness']['tennis']['exterior']
    }

    if (typeof window.gtag !== 'undefined') {
      window.gtag("event", "click", {
        send_to: "G-HMJ3ZBRYHV",
        app_name: "NEO",
        video_tour: vidObj.id
      })
    }
    setCurrentVideo({ video: vidObj, path: vidPath })
    setVideoHasFinished(false)
    setExploreDropdownOpen(false)
    modalRef.current.querySelectorAll('.video-header-inner')[0].focus()
    setShowVideoActions(false)
  }, [currentVideo])

  // useEffect(() => {
  //   if (!!show && data.id == 'FITNESS') {
  //     setCurrentVideo({ video: VideoData['legend']['intro'], path: VideoData['paths']['intro'] })
  //     play()
  //   }
  //   return () => {
  //     stop()
  //   }
  // }, [show, data, VideoData])

  useLayoutEffect(() => {
    const start = getQueryString().start
    if (start) {
      setVideoHasFinished(true)
      play()
    }
  }, [])

  useEffect(() => {
    if (!windowFocused) {
      stop()
    }
  }, [windowFocused])


  useEffect(() => {
    setIndex(0)
    if (modalRef.current) {
      // add all the elements inside modal which you want to make focusable
      const focusableElements = 'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])';
      const firstFocusableElement = modalRef.current.querySelectorAll(focusableElements)[0]; // get first element to be focused inside modal
      const focusableContent = modalRef.current.querySelectorAll(focusableElements);
      const lastFocusableElement = focusableContent[focusableContent.length - 1]; // get last element to be focused inside modal

      // console.log( firstFocusableElement )
      document.addEventListener('keydown', (e) => modalKeyDown(e, firstFocusableElement, lastFocusableElement));
      setTimeout(() => {
        if (modalRef.current) {
          modalRef.current.focus()
        }
      }, 500)
    }
    return () => {
      document.removeEventListener('keydown', (e) => modalKeyDown(e, firstFocusableElement, lastFocusableElement));
    };
  }, [show])

  return (
    <AnimatePresence>
      {show && (
        <motion.div
          key="modal"
          ref={modalRef}
          className={`modal ${modalFull ? 'full' : ''} ${isMobile ? 'touch' : ''}`}
          role="dialog"
          tabIndex="100"
          initial={getAxisInitial()}
          animate={getAxisAnimate()}
          exit={getAxisExit()}
          transition={{
            type: "ease",
            ease: 'easeInOut',
            duration: 0.4
          }}
        >

          {!modalFull && (
            <>
              <h1>{data.name}</h1>
              {data.title ? <h3>{data.title}</h3> : ``}
              <p dangerouslySetInnerHTML={{ __html: data.description }}></p>

              {data.id === 'CHILDREN' && (
                <div>
                  <div className="responsive-iframe">
                    <iframe src="https://player.vimeo.com/video/939726285?h=f3e7f26069&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" allow="autoplay; fullscreen; picture-in-picture; clipboard-write" title="Little Squiggles Children’s Enrichment Center"></iframe>
                  </div>
                  <script src="https://player.vimeo.com/api/player.js"></script>
                </div>
              )}
              {data.id !== 'CHILDREN' && (
                <div className="modal-gallery">
                  <div className="modal-gallery-images">
                    {data.images ? (
                      data.images.map((img, i) => {
                        return <Image key={i} src={img.image} alt={img.alt} className={i === index ? 'active' : ''} />
                      })
                    ) : (
                      <div className="coming-soon"><span>Coming Soon</span></div>
                    )}
                  </div>
                  
                  {data.images && data.images.length > 1 ? (<div className="modal-gallery-nav">
                    <button tabIndex="101" className="modal-gallery-nav-prev" onClick={() => handlePrevious()} aria-label="previous"><PrevBtn /></button>
                    <button tabIndex="102" className="modal-gallery-nav-next" onClick={() => handleNext()} aria-label="Next"><NextBtn /></button>
                  </div>) : ``}

                </div>
              )}
            </>
          )}

          {currentVideo.video && modalFull && (
            <div className={`video-wrapper ${currentVideo.video.id}`}>
              <div className="video-responsive-wrapper">

                <VideoPlayer
                  options={{
                    sources: [{
                      src: `/videos/${currentVideo.video.path}`
                    }]
                  }}
                  placeholder={`/videos/${currentVideo.video.placeholder}`}
                  key={currentVideo.video.id}
                  isMuted={isMuted}
                  onPlay={() => {
                    setVideoHasFinished(false)
                    // setVideoFirstFive(true)
                    // setTimeout(() => {
                    //   setVideoFirstFive(false)
                    // }, 5000)
                  }}
                  onEnded={() => {
                    setVideoHasFinished(true)
                  }}
                />
              </div>

              <VideoOverlay classes={`${(videoHasFinished || videoFirstFive || showVideoActions) ? 'done' : ''}`} updateVideo={updateVideo} currentVideo={currentVideo} setCurrentVideo={setCurrentVideo} showVideoActions={showVideoActions} exploreDropdownOpen={exploreDropdownOpen} setExploreDropdownOpen={setExploreDropdownOpen} />

              <button onClick={() => {
                if (isMuted && windowFocused) play()
                if (!isMuted) pause()
                setIsMuted(!isMuted)
              }}
                aria-label={`${isMuted ? 'Turn audio on' : 'Turn audio off'}`}
                className="video-toggle-sound">
                {!isMuted ? (
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                    <g transform="matrix(0.8571428571428571,0,0,0.8571428571428571,0,0)">
                      <path fill="currentColor" d="M12,2.21a.75.75,0,0,0-1.06,0,.75.75,0,0,0,0,1.06,5.26,5.26,0,0,1,0,7.46.75.75,0,0,0,0,1.06.75.75,0,0,0,1.06,0,6.74,6.74,0,0,0,0-9.58Z" ></path>
                      <path fill="currentColor" d="M10.34,4.89a.75.75,0,0,0-1.23.85,2.23,2.23,0,0,1,0,2.52.74.74,0,0,0,.19,1,.75.75,0,0,0,1-.19,3.7,3.7,0,0,0,0-4.22Z" ></path>
                      <path fill="currentColor" d="M6.69,1a1.35,1.35,0,0,0-.8.27L3.07,4.42a.27.27,0,0,1-.19.08H1.75A1.76,1.76,0,0,0,0,6.25v1.5A1.76,1.76,0,0,0,1.75,9.5H2.88a.27.27,0,0,1,.19.08l2.82,3.15a1.35,1.35,0,0,0,.8.27A1.31,1.31,0,0,0,8,11.69V2.31A1.31,1.31,0,0,0,6.69,1Z" ></path>
                    </g>
                  </svg>
                ) : (
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                    <g transform="matrix(0.8571428571428571,0,0,0.8571428571428571,0,0)">
                      <path fill="currentColor" d="M6.41,10.78a.25.25,0,0,0,0,.34l2.16,2.41a1.35,1.35,0,0,0,.8.27,1.31,1.31,0,0,0,1.31-1.31V7.12a.26.26,0,0,0-.43-.18Z" ></path>
                      <path fill="currentColor" d="M.26,13.81h0a.76.76,0,0,0,1,0l12.5-12.5A.76.76,0,0,0,13.94,1,.71.71,0,0,0,14,.76a.73.73,0,0,0-.19-.49l0,0a.74.74,0,0,0-1.06,0l-2,2a.26.26,0,0,1-.19.07.25.25,0,0,1-.18-.08,1.23,1.23,0,0,0-1-.45,1.29,1.29,0,0,0-.8.27L5.74,5.21a.23.23,0,0,1-.19.09H4.42A1.75,1.75,0,0,0,2.67,7.05v1.5A1.78,1.78,0,0,0,3,9.61a.25.25,0,0,1,0,.33L.22,12.73a.75.75,0,0,0,0,1.06Z" ></path>
                    </g>
                  </svg>
                )}
              </button>
            </div>
          )}

          <button tabIndex="103" onClick={() => {
            closeModal()
          }} className="modal-close" aria-label="Close and go back to map"><CloseBtn /></button>
          {(isMobile && windowSize.width > 769 && modalFull) && (
            <button disabled={videoHasFinished} tabIndex="104" aria-label="Toggle map info overlay" className="video-info-toggle" onClick={() => setShowVideoActions(!showVideoActions)}>
              {showVideoActions ? 'Hide Info' : 'Show Info'}
            </button>
          )}
        </motion.div>
      )}
    </AnimatePresence>
  )
}

export default Modal
