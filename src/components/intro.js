import React, {useState, useEffect} from "react"
import { motion } from "framer-motion"
import Image from "../components/image"

const Intro = ({loadMapDetails, loaded}) => {

    const variants = {
        open: { opacity: 1, x: 0 },
        closed: { 
            opacity: 0, 
            transitionEnd: {
                display: "none",
            } 
        }
    }

    useEffect(() => {
        setTimeout(() => {
            loadMapDetails(true)
        }, 800)
    }, [])


    return(
        <motion.div 
            className="intro"
            animate={loaded ? "closed" : "open"}
            variants={variants}
            transition={{ 
                type: "spring", 
                stiffness: 20,
                damping: 10,
                mass: 0.2
            }}
            >
            {/* <Image src="hq-map-intro.jpg" className="intro-img"/> */}
            <Image src="hq-map-main-2023.jpg" className="intro-img"/>
            <div className="intro-inner">

                <motion.img
                    animate={{ x: ["-20vw", "60vw"], opacity: [0, 0.8, 0.8, 0.8, 0.8, 0] }}
                    transition={{ loop: Infinity, ease: "linear", duration: 60 }}
                    initial={{ opacity: 0 }}
                    exit={{ opacity: 0 }}
                    src={`images/cloud-top-right.png`}
                    className="cloud cloud1"
                    style={{ position: "absolute", top: "0vh", maxHeight: `30vh` }}
                />
                <motion.img
                    animate={{ x: ["0vw", "100vw"], opacity:  [0, 0.8,  0.8, 0.8, 0.8, 0] }}
                    transition={{ loop: Infinity, ease: "linear", duration: 30 }}
                    initial={{ opacity: 0 }}
                    exit={{ opacity: 0 }}
                    src={`images/cloud-top-left.png`}
                    className="cloud cloud1"
                    style={{ position: "absolute", top: "0vh", maxHeight: `30vh` }}
                />
                <motion.img
                    animate={{ x: ["60%", "-100%"], opacity: [0, 0.8,  0.8, 0.8, 0.8, 0] }}
                    transition={{ loop: Infinity, ease: "linear", duration: 40 }}
                    initial={{ opacity: 0 }}
                    exit={{ opacity: 0 }}
                    src={`images/cloud-bottom.png`}
                    className="cloud cloud1"
                    style={{ position: "absolute", bottom: "0vh", maxHeight: `40vh` }}
                />
                {/*  
                <h1>Our New Home <small>In Bentonville, Arkansas</small></h1>
                <img className="spark" alt="spark" src={`images/walmart-spark.png`}/>
                <p>View the Interactive Map</p>
                */}
            </div>
        </motion.div>
    )
}

export default Intro