// TODO: when panning turn cursor into grabbie hand
import React, { useState, useEffect, useRef, useLayoutEffect } from "react"
import _ from "lodash"
import Background from "../components/background"
import Modal from "../components/modal"
import Clouds from "../components/clouds"
import Minimap from "../components/minimap"
import SvgComponent from "../components/map"
import Legal from "../components/legal"
import ZoomControls from "../components/zoomControls"
import JSONData from "../data/pois.json"
import { ReactComponent as Compass } from "../assets/compass.svg"
import { TransformWrapper, TransformComponent, ReactZoomPanPinchRef } from "react-zoom-pan-pinch"
import getWindowDimensions from "../useWindowDimensions"
import { browserName, CustomView } from "react-device-detect";
import { RotatingSquare } from "react-loader-spinner"
import useWindowFocus from 'use-window-focus';
import { getQueryString } from "../utils"

export const IndexPage = () => {
  let resetTransformBtn = useRef(null)
  let mapRef = useRef(null)
  /* eslint-disable */
  const [isAnimating, setIsAnimating] = useState(false)
  const [poi, setPoi] = useState(false)
  const [loaded, setLoaded] = useState(false)
  const [currentTabIndex, setCurrentTabIndex] = useState(0)
  const [modalShow, setModalShow] = useState(false)
  const [miniMapHover, setMiniMapHover] = useState(null)
  const [activeItem, setActiveItem] = useState()
  const [scale, setScale] = useState(1)
  const [positionX, setPositionX] = useState(0)
  const [positionY, setPositionY] = useState(0)
  const [canvasX, setCanvasX] = useState(0)
  const [canvasY, setCanvasY] = useState(0)
  const [lockStage, setLockStage] = useState(0)
  const [mapMaxPanX, setMapMaxPanX] = useState(0)
  const [mapMaxPanY, setMapMaxPanY] = useState(0)
  const windowFocused = useWindowFocus();

  /* eslint-enable */

  function updateOnStop(stats) {
    console.log(stats)
    setLockStage(false)
    setScale(stats.scale)
    setPositionX(stats.positionX)
    setPositionY(stats.positionY)
    setCanvasX(stats.positionX)
    setCanvasY(stats.positionY)
    setMiniMapHover(null)
  }

  function updateOnStart(stats) {
    setLockStage(true)
    setScale(stats.scale)
    setPositionX(stats.positionX)
    setPositionY(stats.positionY)
    setCanvasX(stats.positionX)
    setCanvasY(stats.positionY)
  }

  const windowSize = getWindowDimensions()
  const positionRatio = (windowSize) ? (8000 / 4504 * windowSize.height) : 1
  let defaultPosX = (windowSize && windowSize.width < positionRatio) ? -((positionRatio - windowSize.width) / 2) : 0
  const centerXOut = (windowSize && windowSize.width < positionRatio) ? -((positionRatio - windowSize.width)/2) : 0


  function removeClassesFromPOI() {
    const activePOI = document.querySelector(".active_poi")
    if (activePOI) activePOI.classList.remove("active_poi")
  }

  /**
* Simulate a click event.
* @public
* @param {Element} elem  the element to simulate a click on
*/
  var simulateClick = function (elem) {
    // Create our event (with options)
    var evt = new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
      view: window
    });
    // If cancelled, don't dispatch our event
    var canceled = !elem.dispatchEvent(evt);
  };


  useLayoutEffect(() => {
    const start = getQueryString().start
    if (start) {
      setTimeout(() => {
        const trig = document.querySelector('#FITNESS')
        simulateClick(trig)
      }, 1000);
    }
  }, [])

  useEffect(() => {
    function handleResize(positionX) {
      // Set window width/height to state
      if (!windowSize || !mapRef.current.clientWidth || !positionX) return
      if (positionX - mapRef.current.clientWidth < windowSize.width) {
        if (resetTransformBtn) {
          resetTransformBtn.click()
        }
      }
    }

    // Add event listener
    window.addEventListener("resize", handleResize);

    // Call handler right away so state gets updated with initial window size
    handleResize();

  }, [windowSize])



  // useEffect(() => {
  //   if (!windowFocused) closeModal()
  // }, [windowFocused])

  function handleZoom(e, setTransform, positionX, positionY) {
    const clickedEl = e.target || null
    const width = windowSize.width
    const height = windowSize.height
    removeClassesFromPOI()

    if (clickedEl) {
      const svgPrefix = "prefix__"
      let poiID = (clickedEl.getAttributeNode("id")) ? clickedEl.getAttributeNode("id").value : null
      poiID = (poiID == null) ? ((e.target.closest('g.structure') ? e.target.closest('g.structure').getAttributeNode("id").value : null)) : poiID
      if (!poiID) {
        setModalShow(false)
        setPoi(null)
      } else {
        const POILookupVal = poiID.replace(svgPrefix, "")
        if (typeof window.gtag !== 'undefined') {
          window.gtag("event", "click", {
            send_to: "G-HMJ3ZBRYHV",
            app_name: "NEO",
            poi: POILookupVal
          })
        }
        const POIData = _.find(JSONData.pois, x => x.id === POILookupVal)
        if (POIData) {
          const poiEl = document.getElementById(poiID)
          const poiElRect = poiEl.getBoundingClientRect()
          poiEl.classList.add("active_poi")
          setActiveItem(POIData.id)
          setPoi(POIData)
          setModalShow(true)
          setCurrentTabIndex(poiEl.tabIndex)
          let offsetLeft
          let offsetTop

          if (transformComponentRef.current.instance.transformState.scale < 2) {
            const offsetRatioX = poiElRect.x / width
            const offsetRatioY = poiElRect.y / height
            const updatedOffsetX = ((width) - (poiElRect.width * 2.5)) / 2
            const updatedOffsetY = ((height) - (poiElRect.height * 2.5)) / 2
            const newEdgeX = (width * 2.5) * offsetRatioX
            const newEdgeY = (height * 2.5) * offsetRatioY
            if (width <= 768) {
              offsetLeft = -newEdgeX + updatedOffsetX + (positionX * 2.5) + (width)
              offsetTop = -newEdgeY + updatedOffsetY + (positionY * 2.5) - (height * .5 * .5) + (poiElRect.height)
            } else {
              offsetLeft = -newEdgeX + updatedOffsetX + (positionX * 2.5) + (width * .45 * .5) - (poiElRect.width * .5)
              offsetTop = -newEdgeY + updatedOffsetY + (positionY * 2.5) + (height * .5) - (poiElRect.height * .5)
            }

            offsetLeft = (offsetLeft >= 0) ? 0 : offsetLeft
            offsetTop = (offsetTop >= 0) ? 0 : offsetTop

            const mapStage = document.getElementById('stage')
            const mapMaxPanX = windowSize.width - (mapStage.clientWidth * 2.5)
            const mapMaxPanY = windowSize.height - (mapStage.clientHeight * 2.5)

            if (mapMaxPanX < 0) {
              offsetLeft = (mapMaxPanX > offsetLeft) ? mapMaxPanX : offsetLeft
            }
            if (mapMaxPanY < 0) {
              offsetTop = (mapMaxPanY > offsetTop) ? mapMaxPanY : offsetTop
            }

            if (!offsetLeft && !offsetTop) {
              // error case maybe will fix ms surface issue
              const quads = document.getElementById(`quadrants`)
              const q = quads.getBoundingClientRect()
              let x
              const offsetRatioX = q.x / width
              const offsetRatioY = q.y / height
              const updatedOffsetX = ((width) - (q.width * 2.5)) / 2
              const updatedOffsetY = ((height) - (q.height * 2.5)) / 2
              const newEdgeX = (width * 2.5) * offsetRatioX
              const newEdgeY = (height * 2.5) * offsetRatioY
              x = -newEdgeX + updatedOffsetX
              offsetTop = -newEdgeY + updatedOffsetY
              offsetLeft = (windowSize && width < positionRatio) ? -(positionRatio) : x
            }

            setTimeout(() => {
              setPositionX(offsetLeft)
              setPositionY(offsetTop)
              setScale(2.5)
              setIsAnimating(true)
              setTimeout(() => {
                setIsAnimating(false)
              }, 800)
              setTransform(offsetLeft, offsetTop, 2.5, 800, "easeInOutQuad")
            }, 10)
          }
        } else {
          setModalShow(false)
        }
      }
    }
  }

  function miniMapTransform(e, setTransform) {

    if (!miniMapHover) return

    setIsAnimating(true)
    setTimeout(() => {
      setIsAnimating(false)
    }, 800)

    const width = windowSize.width
    const height = windowSize.height
    const quadPath = document.getElementById(`quadrant${miniMapHover.id}`)
    const poiElRect = quadPath.getBoundingClientRect()

    let offsetLeft
    let offsetTop

    if (transformComponentRef.current.instance.transformState.scale > 2) {

      offsetLeft = positionX - (poiElRect.x - width / 2) - (poiElRect.width / 2)
      offsetTop = positionY - (poiElRect.y - height / 2) - (poiElRect.height / 2)
      offsetLeft = offsetLeft < -(width * 2.5) ? -(width * 2.5) : offsetLeft
      offsetTop = offsetTop < -(height * 2.5) ? -(height * 2.5) : offsetTop
      offsetLeft = (offsetLeft >= 0) ? 0 : offsetLeft
      offsetTop = (offsetTop >= 0) ? 0 : offsetTop

      console.log(`offsetLeft: ${offsetLeft}, offsetTop: ${offsetTop}`)
      setTimeout(() => {
        setPositionX(offsetLeft)
        setPositionY(offsetTop)
        setScale(2.5)
        setIsAnimating(true)
        setTimeout(() => {
          setIsAnimating(false)
        }, 800)
        setTransform(offsetLeft, offsetTop, 2.5, 800, "easeInOutQuad")
      }, 10)
    } else {
      // set these values before scale is finished so we have the correct max positions

      const offsetRatioX = poiElRect.x / width
      const offsetRatioY = poiElRect.y / height
      const updatedOffsetX = ((width) - (poiElRect.width * 2.5)) / 2
      const updatedOffsetY = ((height) - (poiElRect.height * 2.5)) / 2
      const newEdgeX = (width * 2.5) * offsetRatioX
      const newEdgeY = (height * 2.5) * offsetRatioY
      let offsetLeft = -newEdgeX + updatedOffsetX + (defaultPosX * 2.5)
      let offsetTop = -newEdgeY + updatedOffsetY
      offsetLeft = (offsetLeft >= 0) ? 0 : offsetLeft
      offsetTop = (offsetTop >= 0) ? 0 : offsetTop

      console.log(`offsetLeft: ${offsetLeft}, offsetTop: ${offsetTop}`)

      setTimeout(() => {
        setPositionX(offsetLeft)
        setPositionY(offsetTop)
        setScale(2.5)
        setIsAnimating(true)
        setTimeout(() => {
          setIsAnimating(false)
        }, 800)
        setTransform(offsetLeft, offsetTop, 2.5, 800, "easeInOutQuad")
      }, 10)
    }
  }

  const closeModal = (e, setTransform) => {
    setModalShow(false)
    setLockStage(true)
    setPoi(null)
    removeClassesFromPOI()
    setTimeout(() => {
      // wait for modal to close
      setLockStage(false)
      // document.querySelector(`[tabindex="${currentTabIndex}"]`).focus()

      setIsAnimating(true)
      setTimeout(() => {
          setIsAnimating(false)
      }, 800)
      setScale(1)
      setTransform(centerXOut, 0, 1, 800, "easeInOutQuad")
    }, 500)
  }

  const updateActiveItem = (e, id) => {
    if (id === null || id === undefined) {
      if (e === null) {
        if (!modalShow) {
          setActiveItem(null)
        }
      } else {
        let buildingID = (e.target.getAttributeNode("id")) ? e.target.getAttributeNode("id").value : null
        buildingID = (buildingID == null) ? e.target.closest('g.structure').getAttributeNode("id").value : buildingID
        if (buildingID) {
          document.querySelectorAll(`.structure`).forEach(function (element) {
            element.classList.remove(`active`);
          });
          setActiveItem(buildingID)
          if (typeof window.gtag !== 'undefined') {
            window.gtag("event", "click", {
              send_to: "G-HMJ3ZBRYHV",
              app_name: "NEO",
              poi: buildingID
            })
          }
          const group = e.target.closest('g.structure')
          const svg = document.getElementById('map')
          setTimeout(() => {
            group.classList.add(`active`)
          }, 0)
          svg.appendChild(group)
        }
      }
    } else if (id === '') {
      const currentBuilding = document.getElementById(activeItem)
      const buildingGroup = document.getElementById('structures')
      if (currentBuilding) {
        currentBuilding.classList.remove(`active`)
        setTimeout(() => {
          buildingGroup.appendChild(currentBuilding)
        }, 250)
      }
    } else {
      setActiveItem(id)
    }
  }

  const updateScale = scale => {
    setScale(scale)
  }

  useEffect(() => {
    if (loaded) {
      document.querySelector('#map-loader').classList.add('loaded')
    }
  }, [loaded])

  const isAnimatingClass = (isAnimating) ? "is-animating map-wrapper" : "map-wrapper"
  const transformComponentRef = useRef(null);

  return (
    <div className={`${isAnimatingClass}`}>
      <TransformWrapper
        defaultScale={1}
        defaultPositionX={defaultPosX}
        enablePadding={false}
        doubleClick={{
          disabled: true
        }}
        options={{
          maxScale: '2.5',
          limitToWrapper: true,
          limitToBounds: false,
          centerContent: true,
          minPositionX: 0,
          maxPositionX: mapMaxPanX,
          minPositionY: 0,
          maxPositionY: mapMaxPanY
        }}
        scalePadding={{
          disabled: true,
        }}
        wheel={{
          disabled: true,
        }}
        pan={{
          animationTime: 0,
          paddingSize: 0,
          velocity: 0,
        }}
        onPanningStart={updateOnStart}
        onPanningStop={updateOnStop}
        zoomIn={{
          animationTime: 800,
          step: 22,
          animationType: `easeInOutQuad`,
        }}
        zoomOut={{
          animationTime: 800,
          animationType: `easeInOutQuad`,
        }}
        ref={transformComponentRef}
      >
        {({ positionX, positionY, zoomIn, zoomOut, setTransform, scale, resetTransform, ...rest }) => (
          <React.Fragment>
            <div className={`map-container ${transformComponentRef.current && transformComponentRef.current.instance.transformState.scale > 1.5 ? 'zoomed' : ''}`} tabIndex={modalShow ? -1 : 1} aria-label='Campus Map'>
              {loaded ? <Minimap setTransform={setTransform} setMiniMapHover={setMiniMapHover} miniMapHover={miniMapHover} miniMapTransform={miniMapTransform} data={JSONData} /> : ""}

              {loaded ? <ZoomControls
                setTransform={setTransform}
                setPositionX={setPositionX}
                setPositionY={setPositionY}
                setScale={setScale}
                updateScale={updateScale}
                setIsAnimating={setIsAnimating}
                scale={scale} /> : ""}

              <TransformComponent>
                <div id="stage" ref={mapRef} className={((lockStage) && (positionX !== canvasX || positionY !== canvasY)) ? "locked" : ""}>
                  <div
                    className="map"
                    data-quadrant-active={miniMapHover && miniMapHover.id}
                  >
                    <Background src="hq-map-main-2023.jpg" onLoad={() => setLoaded(true)}
                    />

                    <div // eslint-disable-line jsx-a11y/no-static-element-interactions
                      className="overlay-wrapper"
                      style={{ transform: `translate(-${positionX}, -${positionY}) scale(${scale})` }}
                      onClick={e => handleZoom(e, setTransform, positionX, positionY, scale)}
                      onKeyPress={e => handleZoom(e, setTransform, positionX, positionY, scale)}
                    >
                      <SvgComponent
                        preserveAspectRatio="xMinYMin slice"
                        className={loaded ? 'loaded overlay' : 'overlay'}
                        updateActiveItem={updateActiveItem}
                        scale={scale}
                      />
                    </div>
                    {loaded ? <Clouds /> : ""}
                  </div>
                </div>

              </TransformComponent>

              {loaded ? <Compass className="compass" width={80} /> : ""}

              <Legal />
            </div>
            <Modal data={poi} show={modalShow} closeModal={e => closeModal(e, setTransform)} />
            <button style={{ display: "none" }} ref={btn => { resetTransformBtn = btn }} onClick={resetTransform} aria-hidden="true">Reset</button>
          </React.Fragment>
        )}

      </TransformWrapper>
      <CustomView condition={browserName === "Safari" || browserName === "isIE"} viewClassName="sniff">
        <div>For the best experience viewing the map, please use Chrome or Firefox.</div>
      </CustomView>

      {loaded && (
        <div id="map-loader"><RotatingSquare ariaLabel="loading-indicator" color="#ffb700" width={80} />Loading now</div>
      )}
    </div>
  )
}

export default IndexPage
